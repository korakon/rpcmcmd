#!/usr/bin/env python3

import random
try:
    from zeroconf import ServiceBrowser, Zeroconf
    import urwid
    import paramiko
except ModuleNotFoundError:
    print('No install: zeroconf, urwid, paramiko')
    raise SystemExit(1)
import threading
import time

__version__ = 0.03
name_list = []
logflag = True
text_palette = True


class MAKSIM:
    START = 'start'
    ERROR = 'error'
    TAB = 'tab'
    RETURN = 'return'
    NAME = 'name'
    COMMAND = 'command'

    COMMAND_FIND_ATR = 'command_find_atr'
    COMMAND_IN_1_ATR = 'command_in_1_atr'
    COMMAND_IN_2_ATR = 'command_in_2_atr'
    COMMAND_OUT_1_ATR = 'command_out_1_atr'
    COMMAND_OUT_2_ATR = 'command_out_2_atr'
    COMMAND_OUT_TEST_1_ATR = 'command_out_test_1_atr'
    COMMAND_OUT_TEST_2_ATR = 'command_out_test_2_atr'

    END = 'end'

    def __init__(self):
        self.__state = self.START
        self.__return = 'none'

    def __set_name(self, find_name):
        in_name = [x.lower() for x in name_list if find_name in x.lower()]
        if find_name == 'all':
            self.__state = self.NAME
            self.__return = ['all', ]
        elif len(in_name) == 1:
            if find_name == in_name[0]:
                self.__state = self.NAME
                self.__return = in_name[0]
            else:
                self.__state = self.TAB
                self.__return = in_name[0]
        else:
            self.__state = self.ERROR

    def __set_command(self, find_cmd):
        in_cmd = [x for x in ['input', 'output', 'find', 'vim'] if x.startswith(find_cmd)]
        if len(in_cmd) == 1:
            if find_cmd == in_cmd[0]:
                if find_cmd in 'find':
                    self.__state = self.COMMAND_FIND_ATR
                    self.__return = 'find'
                if find_cmd in 'input':
                    self.__state = self.COMMAND_IN_1_ATR
                    self.__return = 'set'
                if find_cmd in 'output':
                    self.__state = self.COMMAND_OUT_1_ATR
                if find_cmd in 'vim':
                    self.__state = self.END
                    self.__return = 'vim'
            else:
                self.__state = self.TAB
                self.__return = in_cmd[0]
        else:
            self.__state = self.ERROR

    # Find command

    def __set_command_find_atr(self, find_name):
        in_name = [x for x in ['on', 'off'] if x.startswith(find_name)]
        if len(in_name) == 1:
            if find_name == in_name[0]:
                self.__state = self.END
            else:
                self.__state = self.TAB
                self.__return = in_name[0]
        else:
            self.__state = self.ERROR

    # Input port command

    def __set_command_in_1_atr(self, find_name):
        in_name = [x for x in ['on', 'off'] if x.startswith(find_name)]
        if len(in_name) == 1:
            if find_name == in_name[0]:
                self.__state = self.COMMAND_IN_2_ATR
            else:
                self.__state = self.TAB
                self.__return = in_name[0]
        else:
            self.__state = self.ERROR

    def __set_command_in_2_atr(self, find_name):
        if find_name.isdigit() and set(find_name) <= set('12'):
            self.__state = self.END
        else:
            self.__state = self.ERROR

    # Output port command

    def __set_command_out_1_atr(self, find_name):
        in_name = [x for x in ['on', 'off', 'test'] if x.startswith(find_name)]
        if len(in_name) == 1:
            if find_name == in_name[0]:
                if find_name in ['on', 'off']:
                    self.__state = self.COMMAND_OUT_2_ATR
                    self.__return = 'set'
                else:
                    self.__state = self.COMMAND_OUT_TEST_1_ATR
                    self.__return = 'test'
            else:
                self.__state = self.TAB
                self.__return = in_name[0]
        else:
            self.__state = self.ERROR

    def __set_command_out_2_atr(self, find_name):
        if find_name.isdigit():
            self.__state = self.END
        else:
            self.__state = self.ERROR

    def __set_command_out_test_1_atr(self, find_name):
        if find_name.isdigit():
            self.__state = self.COMMAND_OUT_TEST_2_ATR
        else:
            self.__state = self.ERROR

    def __set_command_out_test_2_atr(self, find_name):
        if find_name.isdigit() and int(find_name) < 1000:
            self.__state = self.END
        else:
            self.__state = self.ERROR

    def __end(self, wtf):
        self.__state = self.ERROR

    def input(self, argument):
        jimp_table_1 = {
            self.START: self.__set_name,
            self.NAME: self.__set_command,
            self.COMMAND_FIND_ATR: self.__set_command_find_atr,
            self.COMMAND_IN_1_ATR: self.__set_command_in_1_atr,
            self.COMMAND_IN_2_ATR: self.__set_command_in_2_atr,
            self.COMMAND_OUT_1_ATR: self.__set_command_out_1_atr,
            self.COMMAND_OUT_2_ATR: self.__set_command_out_2_atr,
            self.COMMAND_OUT_TEST_1_ATR: self.__set_command_out_test_1_atr,
            self.COMMAND_OUT_TEST_2_ATR: self.__set_command_out_test_2_atr,
            self.END: self.__end
        }

        argument_list = argument.lower().split()

        for argument in argument_list:
            if self.__state == self.TAB:
                self.__state = self.ERROR
                break
            jimp_table_1[self.__state](argument)
            if self.__state == self.ERROR:
                break
        else:
            if self.__state == self.TAB:
                argument_list.remove(argument_list[len(argument_list) - 1])
                argument_list.append(self.__return)
                inputName.set_edit_text(" ".join(argument_list))
                inputName.set_edit_pos(999)
        return self.__state, self.__return
        #return self.__state


class SshRpcm:

    def __init__(self, name):
        self.__name = name
        self.__client = paramiko.SSHClient()
        self.__client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.__host_name = self.__name + '-rpcm.local'

    def connect(self):
        try:
            self.__client.connect(hostname=self.__host_name, username='rpcmadmin', password='rpcmpassword', port=22)
        except paramiko.ssh_exception.NoValidConnectionsError:
            textLine('ERROR connect to ' + self.__name)
        except paramiko.ssh_exception.AuthenticationException:
            textLine('ERROR connect to ' + self.__name)
        except:
            textLine(self.__name + ' неизвестная ошибка')
        else:
            textLine('connect to ' + self.__name)
            self.__pipe = self.__client.invoke_shell()
            self.__stdin = self.__pipe.makefile('wb')
            self.__stdout = self.__pipe.makefile('rb')
            self.wait()

    def disconnect(self):
        # self.__stdout.read()
        self.__stdin.writelines('''exit\n''')
        self.__stdout.close()
        self.__stdin.close()
        self.__client.close()
        textLine(self.__name + ' disconnect')

    def set(self, sense, on_off, port_list):
        port_list = set(str(port_list))
        for port in port_list:
            message = '''set {} {} {} \n'''.format(sense, str(port), on_off)
            self.__stdin.writelines(message)
            self.wait()
        textLine(self.__name + ' set complete')

    def test(self, sense, port_list, cycle):
        port_list = set(str(port_list))
        on_off = 'on'
        for cycle in range(0, int(cycle)*2):
            if on_off == 'on':
                on_off = 'off'
            else:
                on_off = 'on'
            for port in port_list:
                message = '''set {} {} {}\n'''.format(sense, str(port), on_off)
                self.__stdin.writelines(message)
                self.wait()
        textLine(self.__name + ' test complete')

    def find(self, on_off):
        textLine(on_off)
        for port in [0, 9]:
            message = '''set output {} recognition {}\n'''.format(port, on_off)
            self.__stdin.writelines(message)
            self.wait()
        textLine(self.__name + ' find complete')

    def vim(self):
        message = '''set input 1 off\n'''
        self.__stdin.writelines(message)
        self.wait()
        message = '''set input 2 off \n'''
        self.__stdin.writelines(message)
        self.wait()

    def send(self, cmd, v_cmd):
        v_cmd = v_cmd.split()
        textLine(cmd)
        self.connect()
        if cmd == 'find':
            self.find(v_cmd[2])
        elif cmd == 'vim':
            self.vim()
        elif cmd == 'test':
            self.test(v_cmd[1], v_cmd[3], v_cmd[4])
        else:
            self.set(v_cmd[1], v_cmd[2], v_cmd[3])
        self.disconnect()

    def wait(self):
        recv = ''
        while True:
            time.sleep(0.2)
            recv += self.__pipe.in_buffer.empty().decode('utf-8')
            if self.__name.lower() in recv.lower():
                break


class MyListener:
    """
    Add and remove rpcm
    """
    def remove_service(self, zeroconf, type, name):
        # nameList.remove(name.split(',')[0])
        pass

    def add_service(self, zeroconf, type, name):
        info = zeroconf.get_service_info(type, name)
        try:
            if 'RPCM' in info.server:
                name = name.split('.')[0]
                name_list.append(name)
                rpcm = urwid.AttrMap(MenuItem(name), 'main', 'focus')
                rpcmListBox.body.append(rpcm)
                rpcmListBox.body.sort(key=rpcm_sort)
                loop.draw_screen()
        except:
            pass


class MenuItem(urwid.Text):
    """
    class urwid.Text witch method  keypress and selectable

    """
    nameList = []

    def __init__(self, caption):
        self.caption = caption
        urwid.Text.__init__(self, caption)

    def keypress(self, size, key):
        if key == 'enter':
            inputName.set_edit_text(self.caption)
            inputName.set_edit_pos(999)
        if key == 'a':
            name = self.caption
            list_gen = [x.base_widget.text for x in rpcmGroup.body]
            if name not in list_gen:
                rpcmGroup.body.append(urwid.AttrMap(self, 'main', 'focus'))
        elif key == 'd':
            x = [x for x in rpcmGroup.body]
            for i in x:
                if self == i.base_widget:
                    rpcmGroup.body.remove(i)
        else:
            return key

    def selectable(self):
        return True


class InputRpcm(urwid.Edit):
    """
    class urwid.Edit command line
    """
    def __init__(self):
        urwid.Edit.__init__(self)
        urwid.connect_signal(self, 'postchange', self.input_cal)

    def keypress(self, size, key):
        key = urwid.Edit.keypress(self, size, key)
        if key == 'tab':
            edit_text = self.get_edit_text()
            autocomplete = MAKSIM()
            autocomplete.input(edit_text)

        elif key == 'enter':
            validate_cmd = self.get_edit_text()
            validate = MAKSIM()
            stat, cmd = validate.input(validate_cmd)
            rpcm_name = self.get_edit_text().split()[0]
            if stat == 'end' and rpcm_name == 'all':
                list_gen = [x.base_widget.text for x in rpcmGroup.body]
                #rpcm_dict = {x: y for x, y in enumerate(list_gen)}
                """for name in list_gen:
                    rpcm = SshRpcm(name)
                    rpcm_cal = threading.Thread(target=rpcm.send, args=(cmd, validate_cmd), daemon=True)
                    rpcm_cal.start()"""
                cmd_thread_list = []
                rpcm_object =[]
                for id, name in enumerate(list_gen):
                    rpcm_object.append(SshRpcm(name))
                    cmd_thread_list.append(threading.Thread(target=rpcm_object[id].send, args=(cmd, validate_cmd), daemon=True))
                    cmd_thread_list[id].start()
            elif stat == 'end':
                rpcm = SshRpcm(rpcm_name)
                rpcm_cal = threading.Thread(target=rpcm.send, args=(cmd, validate_cmd), daemon=True)
                rpcm_cal.start()
        else:
            return key

    def input_cal(self, wid, text, ):
        """
        sort rpcm
        """
        rec = []

        find_name = self.get_edit_text().lower()
        # if len(find_name) > 0:
        #    find_name.split()[0]
        for c in name_list:
            if find_name in c.lower():
                rec.append(c)
        if len(rec) == 0:
            rec.append('None')
            rpcmListBox.body.clear()
        else:
            rpcmListBox.body.clear()
            add = rpcmListBox.body.append
            attr = urwid.AttrMap
            for item in rec:
                rpcm = MenuItem(item)
                add(attr(rpcm, 'main', 'focus'))

            rpcmListBox.body.sort(key=rpcm_sort)


class HelpLay:
    _helpText = '''Горячие клавиши:
    
    'tab' - автодополнение имен и команд.
    'enter' - автодополнение и ввод, если команда будет валидна.
    'a' - добавить в группу для кооперативной работы.
    'd' - удалить из группы для кооперативной работы

    Комманды:
    
    Поиск:
        name-rpcm find on/off
        
    Альтернативный поиск:
        name-rpcm vim - пищать и все портить
        
    Управление вводом:
        name-rpcm input on/off номер входа.
        name-rpcm input 1 off
    
    Управление выходами:
        name-rpcm output on/off номера выходов.
        name-rpcm output on 12345
    
        name-rpcm output test номера выходов, колличество циклов
        name-rpcm output test 148902 40
    
    Если команду необходимо применить для устройств, добавленных в группу, вместо имени нужно ввести  'all' '''
    _helpStatus = False

    def __init__(self):
        self._content = urwid.Text(self._helpText)
        self._body = urwid.Filler(self._content)
        self._footer = urwid.Text('Exit F1', align='center')
        self._lay = urwid.Frame(self._body, footer=self._footer)

    def swithlay(self):
        if not self._helpStatus:
            loop.widget = self._lay
            self._helpStatus = True
        else:
            loop.widget = main
            self._helpStatus = False


zeroconf = Zeroconf()
listener = MyListener()
browser = ServiceBrowser(zeroconf, "_http._tcp.local.", listener)  # start zeroconf


def rpcm_sort(item):
    return item.base_widget.caption


def unhandled_input(key):
    """
    No  target key press
    """
    global logflag
    if key == 'esc':
        zeroconf.close()
        raise urwid.ExitMainLoop()
    elif key == 'f1':
        help.swithlay()
    elif key == 'f2':
        while True:
            if len(name_list) == 1:
                break
            else:
                name = str(random.randint(10, 9999)) + '_item_RPCM'
                name_list.append(name)
        for item in name_list:
            rpcm = urwid.AttrMap(MenuItem(item), 'main', 'focus')
            rpcmListBox.body.append(rpcm)
        rpcmListBox.body.sort(key=rpcm_sort)
    elif key == 'f3':
        if logflag:
            mainBox.base_widget.widget_list.remove(logLay)
            logflag = not logflag
        else:
            mainBox.base_widget.widget_list.append(logLay)
            logflag = not logflag
    elif key == 'f4':
        logList.body.append(urwid.Text('lol'))


palette = [
    ('main', '', '', '', '#0dd', 'g19'),
    ('focus', '', '', '', '#d80', 'g19'),
    ('boxAct', '', '', '', '#ada', ''),
    ('text_w', '', '', '', 'g23', 'g78'),
    ('text_d', '', '', '', 'g35', 'g93'),]


# Tui Primitive
def atr(x):
    return urwid.AttrMap(x, 'main', 'focus')


def textLine(message):
    """
    Message to logLay
    """
    global text_palette

    def atr(x):
        if text_palette:
            atr = 'text_w'
        else:
            atr = 'text_d'
        return urwid.AttrMap(x, atr)
    tmp = time.localtime()
    text_send = '{}:{}:{} {}'.format(tmp[3], tmp[4], tmp[5], message)
    logList.body.append(atr(urwid.Text(text_send)))
    end_focus = logList.body.positions().stop - 1
    logList.body.set_focus(end_focus)
    text_palette = not text_palette
    loop.draw_screen()


help = HelpLay()
logList = urwid.ListBox(urwid.SimpleFocusListWalker([]))
inputNameHeader = urwid.Text('Input Name')
inputName = InputRpcm()
rpcmGroup = urwid.ListBox(urwid.SimpleFocusListWalker([]))
rpcmListBox = urwid.ListBox(urwid.SimpleFocusListWalker([]))


# Tui box
logLay = atr(urwid.LineBox(logList, 'Log '))
leftBox = urwid.Pile([inputNameHeader, atr(inputName),
                      urwid.Divider(div_char='-'), (20, rpcmGroup)])
leftM = atr(urwid.LineBox(urwid.Filler(leftBox, valign='top')))
rightM = atr(urwid.LineBox(rpcmListBox, 'RPCM list'))
mainBox = urwid.AttrMap(urwid.Columns([leftM, rightM, logLay]), 'main')


# Tui Main


main_header = urwid.Text('RPCM commander   version = {}'
                         .format(__version__), align='center')
main_footer = urwid.Text('Press F1 help')
main = urwid.Frame(mainBox, header=main_header, footer=main_footer)
loop = urwid.MainLoop(main, palette, unhandled_input=unhandled_input)
loop.screen.set_terminal_properties(colors=256)

if __name__ == '__main__':
    loop.run()
